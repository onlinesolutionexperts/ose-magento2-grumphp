# Changelog
        
## [1.4.0] - 2024-08-01 


### Added

### Changed

### Deprecated

### Removed

### Fixed

        
## [1.3.0] - 2024-08-01 


### Added

### Changed

### Deprecated

### Removed

### Fixed

        
## [1.2.5] - 2024-07-30 


### Added

### Changed

### Deprecated

### Removed

### Fixed

        
## [1.2.4] - 2023-09-15 
- Bump phpcs version


### Added

### Changed
- composer.lock

### Deprecated

### Removed

### Fixed

        
## [1.2.3] - 2023-09-08 
- Bump phpcs version


### Added

### Changed
- composer.lock

### Deprecated

### Removed

### Fixed

        
## [1.2.2] - 2023-08-28 
- update composer.lock


### Added

### Changed
- composer.lock

### Deprecated

### Removed

### Fixed

        
## [1.2.1] - 2023-08-28 
- Remove PHPStan


### Added

### Changed
- composer.json
- composer.lock
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [1.2.0] - 2023-08-25 
- Update phpcs and fix license
- Update to new magento2-phpcs package


### Added

### Changed
- composer.json
- composer.lock

### Deprecated

### Removed

### Fixed

        
## [1.1.3] - 2023-08-21 
- Change regexp type


### Added

### Changed
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [1.1.2] - 2023-08-21 
- Remove excludes


### Added

### Changed
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [1.1.1] - 2023-08-18 
- Add exclude for Magento and data patches


### Added

### Changed
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [1.1.0] - 2023-08-18 
- update dependencies


### Added

### Changed
- composer.lock

### Deprecated

### Removed

### Fixed

        
## [1.0.1] - 2023-08-04 
- Fix markdown


### Added

### Changed
- README.md

### Deprecated

### Removed

### Fixed

        
## [1.0.0] - 2023-08-04 
- Update phpcs package


### Added

### Changed
- composer.json
- composer.lock

### Deprecated

### Removed

### Fixed

        
## [0.9.0] - 2023-08-04 
- Bump phpcs ruleset


### Added

### Changed
- composer.json

### Deprecated

### Removed

### Fixed

        
## [0.8.0] - 2023-08-04 
- Re-enable PHPStan
- Add readme


### Added
- README.md

### Changed
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [0.7.0] - 2023-08-04 
- Remove PHPStan


### Added

### Changed
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [0.6.0] - 2023-08-04 
- Add configuration file


### Added

### Changed
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [0.5.0] - 2023-08-03 
- Remove unneeded repository


### Added

### Changed
- composer.json

### Deprecated

### Removed

### Fixed

        
## [0.4.0] - 2023-07-17 
- Remove PHP CS Fixer


### Added

### Changed
- composer.json
- src/grumphp.yml

### Deprecated

### Removed

### Fixed

        
## [0.3.0] - 2023-07-17 
- Update ose phpcs dep
- Remove .php-cs-fixer.dist.php


### Added

### Changed
- composer.json

### Deprecated

### Removed
- src/.php-cs-fixer.dist.php

### Fixed

        
## [0.2.0] - 2023-07-17 
- Change to use .php-cs-fixer.dist.php inside ose/magento2-phpcs


### Added

### Changed
- src/grumphp.yml

### Deprecated

### Removed

### Fixed


## [0.1.0] - 2023-07-13 
Initial setup

