# GRUMPHP
## Installation
```bash
composer require --dev ose/magento2-grumphp
```

DO NOT create a `grumphp.yml` file in the root directory when you are be prompted, delete it if created.

Add the following to your `composer.json`

```bash
  "extra": {
    "grumphp": {
            "config-default-path": "vendor/ose/magento2-grumphp/src/grumphp.yml"
        }
  }
```

and run  `composer install` to apply the changes.

## Usage
- Make sure that your changes are in GIT staging area.
- When you want to commit your changes, type.
```bash
./vendor/bin/grumphp git:pre-commit
```
to run tasks defined in `grumphp.yml` **only** on the code from the commit.
This command will be also run when you try to do a commit - it uses git pre-commit hook.

## Built-in tasks
This package has pre-defined configuration of `grumphp.yml` file, thanks for that you can install
and just use it. Currently, the package contains these tasks :
- [**jsonlint**](https://github.com/phpro/grumphp/blob/master/doc/tasks/jsonlint.md)
- [**xmllint**](https://github.com/phpro/grumphp/blob/master/doc/tasks/xmllint.md)
- [**phplint**](https://github.com/phpro/grumphp/blob/master/doc/tasks/phplint.md)
- [**yamllint**](https://github.com/phpro/grumphp/blob/master/doc/tasks/yamllint.md)
- [**composer**](https://github.com/phpro/grumphp/blob/master/doc/tasks/composer.md)
- [**phpcs**](https://github.com/phpro/grumphp/blob/master/doc/tasks/phpcs.md)
- [**phpmd**](https://github.com/phpro/grumphp/blob/master/doc/tasks/phpmd.md)
- [**phpstan**](https://github.com/phpro/grumphp/blob/master/doc/tasks/phpstan.md) - Currently the level is set to **6**
- [**git_blacklist**](https://github.com/phpro/grumphp/blob/master/doc/tasks/git_blacklist.md) - This task checks if a developer didn't use one of blacklisted words like `var_dump` or `console.log`

## Compatibility
- Magento >= 2.4.2
- PHP version >= 7.2 || >= 8.1